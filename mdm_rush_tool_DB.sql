# --------------------------------------------------------
# Host:                         127.0.0.1
# Server version:               5.6.10
# Server OS:                    Win64
# HeidiSQL version:             6.0.0.3603
# Date/time:                    2014-09-27 20:05:20
# --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

# Dumping database structure for sampledb
DROP DATABASE IF EXISTS `sampledb`;
CREATE DATABASE IF NOT EXISTS `sampledb` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `sampledb`;


# Dumping structure for table sampledb.maindata_metadata
DROP TABLE IF EXISTS `maindata_metadata`;
CREATE TABLE IF NOT EXISTS `maindata_metadata` (
  `id` char(50) NOT NULL,
  `en_name` varchar(50) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `cobon` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# Data exporting was unselected.


# Dumping structure for table sampledb.md_propertity
DROP TABLE IF EXISTS `md_propertity`;
CREATE TABLE IF NOT EXISTS `md_propertity` (
  `mdpid` varchar(50) DEFAULT NULL,
  `mdepid` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `datatype` varchar(50) DEFAULT NULL,
  `length` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# Data exporting was unselected.


# Dumping structure for table sampledb.rush_config
DROP TABLE IF EXISTS `rush_config`;
CREATE TABLE IF NOT EXISTS `rush_config` (
  `id` varchar(50) NOT NULL,
  `maindataname` varchar(50) DEFAULT NULL,
  `samevalue` float DEFAULT NULL,
  `differentvalue` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# Data exporting was unselected.


# Dumping structure for table sampledb.rush_pendingdata
DROP TABLE IF EXISTS `rush_pendingdata`;
CREATE TABLE IF NOT EXISTS `rush_pendingdata` (
  `id` varchar(50) DEFAULT NULL,
  `rushid` varchar(50) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `maindata1` varchar(50) DEFAULT NULL,
  `maindata2` varchar(50) DEFAULT NULL,
  `info` varchar(200) DEFAULT NULL,
  `comparevalue` double DEFAULT NULL,
  `dt` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# Data exporting was unselected.


# Dumping structure for table sampledb.rush_propertity_config
DROP TABLE IF EXISTS `rush_propertity_config`;
CREATE TABLE IF NOT EXISTS `rush_propertity_config` (
  `id` varchar(50) DEFAULT NULL,
  `configid` varchar(50) DEFAULT NULL,
  `itemname` varchar(50) DEFAULT NULL,
  `proporation` float DEFAULT NULL,
  `algname` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# Data exporting was unselected.


# Dumping structure for table sampledb.rush_report
DROP TABLE IF EXISTS `rush_report`;
CREATE TABLE IF NOT EXISTS `rush_report` (
  `id` varchar(50) DEFAULT NULL,
  `info` longtext,
  `maindataname` varchar(50) DEFAULT NULL,
  `algname` varchar(50) DEFAULT NULL,
  `time` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# Data exporting was unselected.


# Dumping structure for table sampledb.t_login_log
DROP TABLE IF EXISTS `t_login_log`;
CREATE TABLE IF NOT EXISTS `t_login_log` (
  `login_log_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `ip` varchar(23) DEFAULT NULL,
  `login_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`login_log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# Data exporting was unselected.


# Dumping structure for table sampledb.t_user
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE IF NOT EXISTS `t_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(30) DEFAULT NULL,
  `credits` int(11) DEFAULT NULL,
  `password` varchar(32) DEFAULT NULL,
  `last_visit` datetime DEFAULT NULL,
  `last_ip` varchar(23) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# Data exporting was unselected.
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
