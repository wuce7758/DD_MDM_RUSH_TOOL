package maindatart.maindata;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * 
 * @author zhangxu	
 * 
 * 主数据数据容器
 *
 */
public class MainDataContener {
	
	Map<String,Object> items = new HashMap<>();
    
    
    /**
     * 设定主数据数值
     * @param name
     * @param value 
     */
    public void SetValue(String name,Object value){
        items.put(name, value);
    }
    
    public void SetValues(Map<String,Object> values){
    	items.putAll(values);
    }
   
    
    /**
     * 获取数据项数值
     * @return 
     */
    public Object getValue(String itemName){
        
       return items.get(itemName);
    }
    
    /**
     * 获取名称
     * @return
     */
    public Set<String> getNames(){
    	return items.keySet();
    }
}
