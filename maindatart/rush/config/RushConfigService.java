/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package maindatart.rush.config;

import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author dingding
 * 
 * 对主数据清洗算法的配置类
 * 
 */
public class RushConfigService {
    
    ConfigDao dao = new ConfigDao();
    
    /**
     * 根据主数据名称获取所有主数据对应清洗算法
     * @param maindataname
     * @return 
     */
     public List<MainDataRashConfig> getMainDataRashConfigs(String maindataname)throws Exception{
        try {
            return dao.getMainDataRashConfigByMainDataName(maindataname);
        } catch (SQLException ex) {
            Logger.getLogger(RushConfigService.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
     } 
     
     public MainDataRashConfig getMainDataRashConfig(String configID) throws Exception{
         try {
            return dao.getMainDataRashConfigByID(configID);
        } catch (SQLException ex) {
            Logger.getLogger(RushConfigService.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
         
     }
     
     public void addMainDataRashConfig(MainDataRashConfig mdrc) throws Exception{
         dao.insert(mdrc);
     }

    
}
