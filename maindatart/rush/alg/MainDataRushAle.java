/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package maindatart.rush.alg;

import java.util.List;
import maindatart.maindata.MainDataContener;
import maindatart.metadata.MainDataMetaDataEntity;
import maindatart.rush.config.MainDataRashConfig;
import maindatart.rush.config.PropertityConfig;


/**
 *
 * @author 丁丁
 * 
 * 算法描述
 *  弄两条主数据，根据匹配算法，计算两条数据相似度。
 * 返回0-1之间的数值。1表示完全一样，0表示完全不一样。
 * 
 */
public class MainDataRushAle {
    public static double same(MainDataContener a,MainDataContener b,MainDataMetaDataEntity mdmd,MainDataRashConfig cfg)throws Exception{
        
        List<PropertityConfig> propertitys = cfg.getItemConfig();
        
        double ret = 0;
        
        for(PropertityConfig pcf : propertitys){
            String alg = pcf.getAlgname();
            String itemname = pcf.getItmename();
            Object aa = a.getValue(itemname);
            Object bb = b.getValue(itemname);
            if(aa == null || bb == null)
                continue;
            double proper = DataSameAlgFactory.getDataSameAlgorithm(alg).itemSame(aa, bb);
            proper = proper * pcf.getProportion();
            ret = ret + proper;
        }
        return ret;
    }
}
